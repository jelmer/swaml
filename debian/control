Source: swaml
Section: web
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 12),
               python,
               dh-python,
X-Python-Version: >= 2.4
Standards-Version: 4.3.0
Homepage: http://swaml.wikier.org
Vcs-Browser: https://salsa.debian.org/phls/swaml
Vcs-Git: https://salsa.debian.org/phls/swaml.git

Package: swaml
Architecture: all
Depends: ${python:Depends},
         ${misc:Depends},
         python-rdflib,
         python-sparqlwrapper,
Suggests: buxon
Description: Semantic Web Archive of Mailing Lists
 SWAML reads a collection of email messages stored in a mailbox
 (from a mailing list compatible with RFC 4155) and generates a
 RDF description. It is written in Python using SIOC as the main
 ontology to represent in RDF a mailing list, including the next
 features:
 .
   * Platform independent.
   * Text-based.
   * Compatible with RFC 4155.
   * Serialize RDF to disk.
   * Reusability of ontologies already extended, mainly SIOC.
   * Enrichment using FOAF.
   * KML support.
